package pw.bowser.command;

import org.spout.api.command.annotated.CommandDescription;
import org.spout.api.command.annotated.Permissible;
import org.spout.api.command.CommandArguments;
import org.spout.api.command.CommandSource;
import org.spout.api.exception.CommandException;

import pw.bowser.SpoutPlugTestPlugin;

/**
 * Provides an example of a class to hold commands.
 */
public class SpoutPlugTestCommands {
	private final SpoutPlugTestPlugin plugin;

	public SpoutPlugTestCommands(SpoutPlugTestPlugin instance) {
		this.plugin = instance;
	}

	/**
	 * Provides an example command that can be issued to the Spout server.
	 * @param source CommandSource
	 * @param args CommandArguments
	 * @throws CommandException
	 */
	@CommandDescription(aliases = { "command", "cmd" }, desc = "This is an example of what a command might look like. Try it out with /cmd !")
	@Permissible("bowser.some.permission")
	public void exampleCommand(CommandSource source, CommandArguments args) throws CommandException {
		// Calling this command will send whoever issued it the message below.
		source.sendMessage("The bowser plugin command has been successfully issued.");
	}
}

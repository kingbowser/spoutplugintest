package pw.bowser;

import org.spout.api.plugin.Plugin;
import org.spout.api.command.annotated.AnnotatedCommandExecutorFactory;

import pw.bowser.command.SpoutPlugTestBaseCommand;
import pw.bowser.command.SpoutPlugTestCommands;
import pw.bowser.configuration.SpoutPlugTestConfiguration;

/**
 * Defines the main class of the plugin.
 */
public class SpoutPlugTestPlugin extends Plugin {
	private static SpoutPlugTestPlugin instance;
	private SpoutPlugTestConfiguration config;

	@Override
	public final void onLoad() {
		setInstance(this);
		config = new SpoutPlugTestConfiguration(getDataFolder());
		config.load();
		getLogger().info("loaded.");
	}

	@Override
	public final void onEnable() {
		// Register Base Command (/command)
		AnnotatedCommandExecutorFactory.create(new SpoutPlugTestBaseCommand(this));
		// Register Commands under Base Command (/command command)
		AnnotatedCommandExecutorFactory.create(new SpoutPlugTestCommands(this), getEngine().getCommandManager().getCommand("command"));

		// Register Events
		getEngine().getEventManager().registerEvents(new SpoutPlugTestListener(this), this);

		getLogger().info("enabled.");
	}

	@Override
	public final void onDisable() {
		getLogger().info("disabled.");
	}

	private static void setInstance(SpoutPlugTestPlugin plugin) {
		instance = plugin;
	}

	public static SpoutPlugTestPlugin getInstance() {
		return instance;
	}

	public final SpoutPlugTestConfiguration getConfig() {
		return config;
	}
}

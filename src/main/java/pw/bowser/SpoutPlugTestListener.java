package pw.bowser;

import org.spout.api.event.EventHandler;
import org.spout.api.event.player.PlayerChatEvent;
import org.spout.api.event.Listener;

/**
 * Provides an example of an event listener class.
 */
public class SpoutPlugTestListener implements Listener {
	private SpoutPlugTestPlugin plugin;

	public SpoutPlugTestListener(SpoutPlugTestPlugin instance) {
		this.plugin = instance;
	}

	@EventHandler
	public void onPlayerChat(PlayerChatEvent event) {
		// Do Something on PlayerChatEvent
	}
}
